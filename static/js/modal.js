var allow_btns = document.getElementsByClassName("btn btn-warning btn-sm allow");
var i;
for(i=0;i<allow_btns.length;i++)
{
  allow_btns[i].onclick = function(){
    var item = $(this).closest("tr").text();
    var list = item.split('\n');
    var modal_body = document.getElementById('allow-user');
    modal_body.value = list[1].trim();
  }
}

var deny_btns = document.getElementsByClassName("btn btn-danger btn-sm deny");
for(i=0;i<deny_btns.length;i++)
{
  deny_btns[i].onclick = function(){
    var item = $(this).closest("tr").text();
    var list = item.split('\n');
    var modal_body = document.getElementById('deny-user');
    modal_body.value = list[1].trim();
  }
}