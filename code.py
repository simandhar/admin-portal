import web
import mainService
import constants as CONSTANTS

urls = (
    '/', 'index',
    '/register', 'register',
    '/test', 'test',
    '/search', 'search'
)

render = web.template.render('templates/')

class test:
    def GET(self):
        return "ok"

    def POST(self):
        return "{'this':1}"

class index:
    def GET(self):
        return render.index(CONSTANTS.MEMBER_TYPE, mainService.get_users())

class register:
    def POST(self):
        mainService.add_user(web.input())
        web.seeother('/')

class search:
    def GET(self):
        #add search logic here
        return "ok";

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()