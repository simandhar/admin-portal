from pymongo import MongoClient


class dbService():

    def __init__(self,db_name,collection_name):
        self.client = MongoClient()
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]
        #insert log here

    def close(self):
        self.client.close()

