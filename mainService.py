from dbService import dbService
import json,hashlib

def get_users():
    dbServer = dbService('test','user')
    records = dbServer.collection.find()
    dbServer.close()
    return records

def add_user(web_input):
    dbServer = dbService('test','user')
    indexDbServer = dbService('test','user_idx')
    user_count = indexDbServer.collection.find_one()
    print user_count
    user = {"_id":int(user_count['count'])+1,
        "name": web_input['name'],
        "fname": web_input['fname'],
        "lname": web_input['lname'],
        "activated":0,
        "type":"user",
        "shortcuts":[],
        "pwd": hashlib.md5(web_input['user_password'].encode('utf-8')).hexdigest()
    }
    print user
    dbServer.collection.insert_one(user)
    dbServer.close()
    user_count['count']+=1
    indexDbServer.collection.update_one({
        '_id':user_count['_id']
    },{
        '$inc': {
            'count':1
        }
    }, upsert=False)
    indexDbServer.close()

